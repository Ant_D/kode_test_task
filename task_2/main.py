from app import create_app, scheduler

app = create_app()
scheduler.start()