import unittest
from flask import render_template
from app import create_app, db, scheduler, mail
from app.models import User, Subscription, Ticker
from app.stock_screener import TickerIsAbsentError
from app.mailing import Mailing
from config import Config


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    MAIL_SERVER = "localhost"
    MAIL_PORT = 8025


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
    
    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()


class ApiTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.client = self.app.test_client()
        self.url = "/subscription"

    def post(self, email=None, ticker=None, min_price=None, max_price=None):
        json = {"email": email, "ticker": ticker,
                "min_price": min_price, "max_price": max_price}
        json = dict(filter(lambda i: i[1] is not None, json.items()))
        return self.client.post(self.url, json=json)

    def delete(self, email=None, ticker=None):
        args = {"email": email, "ticker": ticker}
        args = dict(filter(lambda i: i[1] is not None, args.items()))
        return self.client.delete(self.url, query_string=args)

    def test_unique_constraint(self):
        r = self.post("d@y.ru", "something", min_price=120)
        self.assertEqual(r.status_code, 201)

        r = self.post("d@y.ru", "something", min_price=120)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(Subscription.query.count(), 1)

    def test_correct_post(self):
        # both prices
        r = self.post("d@y.ru", "ticker 1", min_price=1, max_price=2)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(Subscription.query.count(), 1)
        
        # only min_price
        r = self.post("d@y.ru", "ticker 2", min_price=1)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(Subscription.query.count(), 2)

        # only max_price
        r = self.post("d@y.ru", "ticker 3", max_price=2)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(Subscription.query.count(), 3)

    def test_subscription_limit(self):
        limit = self.app.config["SUBSCRIPTIONS_PER_USER"]
        for t in range(limit):
            r = self.post("d@y.ru", str(t), min_price=1)
            self.assertEqual(r.status_code, 201)
            self.assertEqual(Subscription.query.count(), t + 1)
        r = self.post("d@y.ru", "one more ticker", min_price=1)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(Subscription.query.count(), limit)

    def test_post_validation(self):
        # no ticker
        r = self.post("d@y.ru", min_price=1)
        self.assertEqual(r.status_code, 400)

        # no email
        r = self.post(ticker="something", min_price=1)
        self.assertEqual(r.status_code, 400)

        # wrong email
        r = self.post("dy.ru", "something", min_price=120)
        self.assertEqual(r.status_code, 400)

        # no prices
        r = self.post("d@y.ru", "something")
        self.assertEqual(r.status_code, 400)

        # no json
        r = self.client.post("/subscription", data={
            "email": "d@y.ru",
            "ticker": "some ticker",
            "min_price": "120"
        })
        self.assertEqual(r.status_code, 400)

    def test_correct_delete(self):
        # delete one subscription
        self.post("d@y.ru", "ticker", 120)
        r = self.delete("d@y.ru", "ticker")
        self.assertEqual(r.status_code, 200)
        self.assertEqual(Subscription.query.count(), 0)

        # delete all user's subscriptions
        limit = self.app.config["SUBSCRIPTIONS_PER_USER"]
        for t in range(limit):
            self.post("d@y.ru", str(t), min_price=1)
        r = self.delete("d@y.ru")
        self.assertEqual(r.status_code, 200)
        self.assertEqual(Subscription.query.count(), 0)

    def test_delete_validation(self):
        #no email
        r = self.delete(ticker="hmm")
        self.assertEqual(r.status_code, 400)


class MailingTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.mailing = Mailing(self.mock_download_price)

    @staticmethod
    def mock_download_price(ticker):
        data = {
            "ABC": 12
        }
        if data.get(ticker.name) is not None:
            return data[ticker.name]
        raise TickerIsAbsentError(ticker)

    def _test_price(self, template, **kwargs):
        ticker = Ticker(name="ABC")
        user = User(email="d@y.r")
        subs = Subscription(user=user, ticker=ticker, **kwargs)
        db.session.add_all([ticker, user, subs])

        price = self.mock_download_price(ticker)
        price_border = subs.min_price or subs.max_price
        expected = render_template(template, price=price,
            price_border=price_border, ticker=ticker.name)

        with mail.record_messages() as outbox:
            self.mailing()
            self.assertEqual(len(outbox), 1)        
            self.assertEqual(outbox[0].body, expected)

    def test_mailing(self):
        self._test_price("low_price_notice.txt", min_price=15)
        self._test_price("high_price_notice.txt", max_price=10)
    
    def test_ticker_is_absent(self):
        ticker = Ticker(name="BAC")
        user = User(email="d@y.r")
        subs = Subscription(user=user, ticker=ticker, min_price=50)
        db.session.add_all([ticker, user, subs])

        expected = render_template("ticker_is_absent.txt", ticker=ticker.name)

        with mail.record_messages() as outbox:
            self.mailing()
            self.assertEqual(len(outbox), 1)        
            self.assertEqual(outbox[0].body, expected)


if __name__ == "__main__":
    unittest.main()