from flask import render_template
from . import mail


def send_notice(subscription, price, price_border, template):
    mail.send_message(
        subject=render_template(
            "price_notice_title.txt", 
            ticker=subscription.ticker.name
        ),
        recipients=[subscription.user.email],
        body=render_template(
            template, 
            ticker=subscription.ticker.name, 
            price=price, 
            price_border=price_border
        )
    )


def notice_about_high_price(subscription, price):
    send_notice(
        subscription,
        price=price, 
        price_border=subscription.max_price,
        template="high_price_notice.txt"
    )


def notice_about_low_price(subscription, price):
    send_notice(
        subscription,
        price=price, 
        price_border=subscription.min_price,
        template="low_price_notice.txt"
    )


def send_ticker_is_absent(subscription):
    mail.send_message(
        subject=render_template(
            "ticker_is_absent_title.txt", 
            ticker=subscription.ticker.name
        ),
        recipients=[subscription.user.email],
        body=render_template(
            "ticker_is_absent.txt", 
            ticker=subscription.ticker.name
        )
    )