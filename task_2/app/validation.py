import re


class ValidationError(ValueError):
    def __init__(sefl, *args, **kwargs):
        super().__init__(*args, **kwargs)


def isfloat(s: str) -> bool:
    try:
        float(s)
    except:
        return False
    else:
        return True


def isemail(s: str) -> bool:
    r = re.compile("[^@]+@[^@]+\.[^@]+")
    return bool(r.match(s))