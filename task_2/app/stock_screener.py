import requests
from flask import current_app


class RequestLimitExceeded(Exception):
    pass


class TickerIsAbsentError(Exception):
    pass


def download_price(ticker):
    result = requests.get("https://www.alphavantage.co/query", params={
        "function": "GLOBAL_QUOTE",
        "symbol": ticker.name,
        "apikey": current_app.config.get("ALPHAVANTAGE_APIKEY")
    }).json()
    quote = result.get("Global Quote")
    if quote is None:
        raise RequestLimitExceeded
    price_entry = quote.get("05. price")
    if price_entry is None:
        raise TickerIsAbsentError(ticker)
    price = float(price_entry)
        
    return price
