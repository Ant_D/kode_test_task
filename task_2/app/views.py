from flask import Blueprint, Response, request, current_app
from sqlalchemy import and_
from sqlalchemy.exc import SQLAlchemyError
from .models import Subscription, Ticker, User, db, get_or_create
from .errors import bad_request
from .validation import ValidationError


bp = Blueprint("views", __name__)


@bp.route("/subscription", methods=["POST"])
def subscribe():
    data = request.get_json()
    if not data: 
        return bad_request("Provide json format request")
    if data.get("min_price") is None and data.get("max_price") is None:
        return bad_request("Provide min_price and/or max_price")
    if Subscription.get_by_email_and_ticker(data.get("email"), data.get("ticker")):
        return bad_request("Subscription of {} on {} already exists"
            .format(data.get("email"), data.get("ticker")))
    try:
        user = get_or_create(db.session, User, email=data.get("email"))
        threshold = current_app.config["SUBSCRIPTIONS_PER_USER"]
        if len(user.subscriptions) >= threshold:
            return bad_request("User {} reached threshold of {} subscription(s)"\
                .format(user.email, threshold))
        subscription = Subscription(    
            user=user,
            ticker=get_or_create(db.session, Ticker, name=data.get("ticker")),
            min_price=data.get("min_price"),
            max_price=data.get("max_price")
        )   
        db.session.add(subscription)
        db.session.commit()
    except ValidationError as err:
        return bad_request(str(err))
    # except SQLAlchemyError:
    #     return bad_request("Something went wrong")
    return Response(status=201)
    

@bp.route("/subscription", methods=["DELETE"])
def unsubscribe():
    email = request.args.get("email")
    ticker_name = request.args.get("ticker")
    if not email:   
        return bad_request("Provide email")
    if ticker_name:
        subscription = Subscription.get_by_email_and_ticker(
            email=email, 
            ticker=ticker_name
        )
        if subscription:
            db.session.delete(subscription)
            db.session.commit()
    else:
        Subscription.query.filter(
            Subscription.user.has(email=email)
        ).delete(synchronize_session='fetch')
        db.session.commit()
    return Response(status=200)
