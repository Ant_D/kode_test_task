from flask import current_app
from .email import (notice_about_high_price, notice_about_low_price, 
    send_ticker_is_absent)
from .models import Subscription
from .stock_screener import TickerIsAbsentError
from . import scheduler


def notice(s: Subscription, price: float):
    if s.min_price is not None and price < s.min_price:        
        notice_about_low_price(s, price)
    if s.max_price is not None and price > s.max_price:
        notice_about_high_price(s, price)


class Mailing:
    def __init__(self, download_price):
        self.download_price = download_price

    def __call__(self):
        with scheduler.app.app_context():
            for s in Subscription.query.all():
                try:
                    price = self.download_price(s.ticker)
                except TickerIsAbsentError as err:
                    send_ticker_is_absent(s)
                    current_app.logger.warning(repr(err))
                except Exception as err:
                    current_app.logger.error(repr(err))
                else:
                    notice(s, price)
    