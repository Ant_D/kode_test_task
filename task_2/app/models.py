from sqlalchemy import UniqueConstraint, and_, CheckConstraint
from sqlalchemy.orm import validates
from . import db
from .validation import isemail, isfloat, ValidationError


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)

    @validates("email")
    def validate_email(self, key, email):
        if email is None or not isinstance(email, str):
            raise ValidationError("Provide user email")
        elif not isemail(email):
            raise ValidationError("Email has incorrect format")
        return email

    def __repr__(self):
        return "{}({email})".format(self.__class__.__name__, email=self.email)


class Ticker(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)

    @validates("name")
    def validate_name(self, key, name):
        if name is None or not isinstance(name, str) or len(name) == 0:
            raise ValidationError("Provide ticker name")
        return name

    def __repr__(self):
        return "{}({name})".format(self.__class__.__name__, name=self.name)


class Subscription(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"),
                        nullable=False)
    user = db.relationship("User", backref=db.backref("subscriptions", lazy=True))
    ticker_id = db.Column(db.Integer, db.ForeignKey("ticker.id"),
                            nullable=False)
    ticker = db.relationship("Ticker", backref=db.backref("subscriptions", lazy=True))
    min_price = db.Column(db.Float, nullable=True)
    max_price = db.Column(db.Float, nullable=True)
    
    __table_args__ = (
        UniqueConstraint("user_id", "ticker_id", name="_user_ticker_uc"),
    )

    @validates("min_price", "max_price")
    def validate(self, key, price):
        if price is not None:
            if not isfloat(price):
                raise ValidationError("Provide {} to be number".format(key))
            if float(price) <= 0:
                raise ValidationError("Provide {} to be greater than zero".format(key))
        return price

    @classmethod
    def get_by_email_and_ticker(cls, email: str, ticker: str):
        return Subscription.query.filter(
            and_(
                Subscription.user.has(email=email),
                Subscription.ticker.has(name=ticker)
            )
        ).first()

    def __repr__(self):
        return "{}({user}, {ticker}, min_price={min_price}, max_price={max_price})".format(
            self.__class__.__name__,
            user=self.user, 
            ticker=self.ticker,
            min_price=self.min_price,
            max_price=self.max_price
        )


def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if not instance:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
    return instance
