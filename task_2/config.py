import os
from datetime import datetime
from app.mailing import Mailing
from app.stock_screener import download_price


basedir = os.path.abspath(os.path.dirname(__file__))


class Config():
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or \
        "sqlite:///" + os.path.join(basedir, "app.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAIL_SERVER = os.getenv("MAIL_SERVER")
    MAIL_PORT = int(os.getenv("MAIL_PORT") or 25)
    MAIL_USE_SSL = os.getenv("MAIL_USE_SSL") is not None
    MAIL_USE_TLS = os.getenv("MAIL_USE_TLS") is not None
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
    MAIL_DEFAULT_SENDER = ("ticker_app", MAIL_USERNAME)
    ALPHAVANTAGE_APIKEY = os.getenv("ALPHAVANTAGE_APIKEY")
    SUBSCRIPTIONS_PER_USER = 5  # Max number of subscriptions per user
    JOBS = [
        {
            "id": "mailing",
            "func": Mailing(download_price),
            "trigger": "interval",
            "minutes": 30,
            "next_run_time": datetime.now()
        }
    ]