def variants(phrase: str, slots: list):
    if not "{" in phrase or not "}" in phrase:
        yield phrase
    else:
        left = phrase.find("{")
        right = phrase.find("}")
        prefix = phrase[:left]
        entry = phrase[left + 1 : right]
        for suffix in variants(phrase[right + 1:], slots):
            yield prefix + entry + suffix
            for slot in slots:
                yield prefix + slot + suffix


def fit(phrase: str, slots: list, search_string: str) -> bool:
    search_string = search_string.lower()
    for variant in variants(phrase, slots):
        if search_string == variant.lower():
            return True
            
    return False


def phrase_search(object_list: list, search_string: str) -> int:
    for obj in object_list:
        if fit(obj["phrase"], obj["slots"], search_string):
            return obj["id"]
    return 0


if __name__ == "__main__":
    """ 
    len(object) != 0
    object["id"] > 0
    0 <= len(object["phrase"]) <= 120
    0 <= len(object["slots"]) <= 50
    """
    object_list = [
        {"id": 1, "phrase": "Hello world!", "slots": []},
        {"id": 2, "phrase": "I wanna {pizza}", "slots": ["pizza", "BBQ", "pasta"]},
        {"id": 3, "phrase": "Give me your power", "slots": ["money", "gun"]},
        {"id": 4, "phrase": "I wanna {eat} and {drink}", "slots": ["pizza", "BBQ", "pepsi", "tea"]},
        {"id": 5, "phrase": "{}", "slots": ["Meat", "Potato"]}
    ]

    assert phrase_search(object_list, 'I wanna pasta') == 2
    assert phrase_search(object_list, 'Give me your power') == 3
    assert phrase_search(object_list, 'Hello world!') == 1
    assert phrase_search(object_list, 'I wanna nothing') == 0
    assert phrase_search(object_list, 'Hello again world!') == 0
    assert phrase_search(object_list, 'I need your clothes, your boots & your motorcycle') == 0
    assert phrase_search(object_list, "i wanna pizza and drink") == 4
    assert phrase_search(object_list, "potato") == 5