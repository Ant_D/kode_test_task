# Тестовое задание №1 на стажировку Backend KODE 2019.1

Необходимо написать функцию, которая будет принимать первым параметром список объектов

```
{ 
    id: int,
    phrase: str,
    slots: List[str]
}
```
#### Пример
```
[
    {"id": 1, "phrase": "Hello world!", "slots": []},
    {"id": 2, "phrase": "I wanna {pizza}", "slots": ["pizza", "BBQ", "pasta"]},
    {"id": 3, "phrase": "Give me your power", "slots": ["money", "gun"]},
]
```

и вторым параметром строку для поиска в объектах

#### Условия объекта

- Длинна example_object > 0
- ID объектов > 0
- Длинна phrase <= 120
- Длинна массива slots <= 50

```
search_string: str
```
Например, `I wanna pasta`

Функция должна проверить наличие строки поиска в объекте с комбинациями и вернуть `id` объекта в случае совпадения или `0` в случае отсутствия совпадений строки в объекте

В объекте в строке `phrase`, при наличии `{}` и строк в `slots` необходимо сделать все возможные комбинации. 
Например:

```
{"phrase": "I wanna {eat}", "slots": ["pizza", "BBQ", "pasta"]}
```

должен скомбинировать следующие варианты:

```
"I wanna eat"
"I wanna pizza"
"I wanna BBQ"
"I wanna pasta"
```

и сравнить их со строкой поиска.

Функцию необходимо реализовать без сторонних библиотек (только python stdlib)

#### Повышенная сложность

+ Рассмотреть вариант, что список объектов может быть с такими фразами:

```
{"phrase": "I wanna {eat} and {drink}", "slots": ["pizza", "BBQ", "pepsi", "tea"]}
```

+ Реализовать неточный поиск (fuzzy search). 
Например:
```
'I wanna eat' и 'i wanna eat' как строки поиска. Функция должны вернуть один id для них.
```

#### Python file

```python
def phrase_search(object_list: list, search_string: str) -> int:
    ...
    # type your code here


if __name__ == "__main__":
    """ 
    len(object) != 0
    object["id"] > 0
    0 <= len(object["phrase"]) <= 120
    0 <= len(object["slots"]) <= 50
    """
    object = [
        {"id": 1, "phrase": "Hello world!", "slots": []},
        {"id": 2, "phrase": "I wanna {pizza}", "slots": ["pizza", "BBQ", "pasta"]},
        {"id": 3, "phrase": "Give me your power", "slots": ["money", "gun"]},
    ]

    assert phrase_search(object, 'I wanna pasta') == 2
    assert phrase_search(object, 'Give me your power') == 3
    assert phrase_search(object, 'Hello world!') == 1
    assert phrase_search(object, 'I wanna nothing') == 0
    assert phrase_search(object, 'Hello again world!') == 0
    assert phrase_search(object, 'I need your clothes, your boots & your motorcycle') == 0
```